(in-package :common-lisp-user)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (let ((package (find-package :cl-who)))
    ;; Unlock cl-who so we can clobber it... yes bad bad boy... sies!
    ;; Curiously cl-who is only locked for sbcl unless other implementations lock by default?

    ;; Other implementations not tested!!!

    #+allegro (setf (excl:package-lock package) nil
                    (excl:package-definition-lock package) nil)
    #+clisp (setf (ext:package-lock package) nil)
    #+cmucl (setf (ext:package-lock package) nil
                  (ext:package-definition-lock package) nil)
    #+ecl (ext:unlock-package package)
    #+sbcl (sb-ext:unlock-package package)))

(defpackage :cl-naive-who-ext
  (:use :cl :cl-getx)
  (:export

   :atts
   :att
   :body
   :tag-p
   :tag-expander
   :define-tag

   :with-html
   :with-html-to-string

   ;;Utilities
   :lts))

;; Does this hide the evidence??
;; Throws and error saying that the package is violated so cant switch lock on again wtf??
;; #+sbcl (sb-ext:lock-package :cl-who)
