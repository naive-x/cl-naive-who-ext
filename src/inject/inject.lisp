(in-package :cl-naive-who-ext)

(defun parse-forms% (forms)
  (mapcar (function parse) forms))

(defmacro cl-who:with-html-output ((var &optional stream
                                        &rest rest
                                        &key prologue indent)
                                   &body body)
  "ALERT Clobbered Version: Transform the enclosed BODY consisting of HTML as s-expressions
into Lisp code to write the corresponding HTML as strings to VAR -
which should either hold a stream or which'll be bound to STREAM if
supplied.

Pre-processing is introduced by naive-who-ext for the body only. That means no pre-processing is done for fmt, esc and str.

Pre-processing is used to expand custom tags to the standard html tags see tag-expander method documentations.
"
  (declare (ignore prologue))
  (multiple-value-bind (declarations forms) (cl-who::extract-declarations body)
    `(let ((,var ,(or stream var)))
       ,@declarations
       (check-type ,var stream)
       (macrolet ((cl-who:htm (&body body)
                    `(cl-who:with-html-output (,',var nil :prologue nil :indent ,,indent)
                       ,@body))
                  (cl-who:fmt (&rest args)
                    `(format ,',var ,@args))
                  (cl-who:esc (thing)
                    (cl-who::with-unique-names (result)
                      `(let ((,result ,thing))
                         (when ,result (write-string (cl-who:escape-string ,result) ,',var)))))
                  (cl-who:str (thing)
                    (cl-who::with-unique-names (result)
                      `(let ((,result ,thing))
                         (when ,result (princ ,result ,',var))))))
         ,@(apply 'cl-who::tree-to-commands
                  (parse-forms% forms)
                  var rest)))))

