(in-package :cl-naive-who-ext)

(defun process-tag (sexp)
  (declare (optimize speed space))
  "Returns values tag, attributes (plist)) and body. T"
  (let ((tag (first sexp))
        (attrs)
        (body))
    (loop :for rest on (cdr sexp) by #'cddr
          :if (keywordp (first rest))
            :do (push (first rest) attrs)
                (push (second rest) attrs)
          :else
            :do (setf body rest))
    (values tag (reverse attrs) body)))

(defun parse (sexp)
  "Parses the sexp and exp"
  (cond ((atom sexp)
         sexp)
        ((and (consp sexp) (tag-p (car sexp)))
         (multiple-value-bind (tag attrs body) (process-tag sexp)
           (when (tag-p tag)
             (let* ((body%           (mapcar (function parse) body))
                    (filled-template (tag-expander tag attrs body%)))
               (parse filled-template)))))
        ((consp sexp)
         (mapcar (function parse) sexp))))

(defgeneric tag-p (tag))

(defmethod tag-p (tag)
  (declare (ignorable tag))
  nil)

(defgeneric tag-expander (tag atts body)
  (:documentation "Tag expander method. Specialize with (eql :tag).

Tag must be a KEYWORD, the parser depends on it! Be carefull not to
clobber normal html tags like :div if you intend on sharing your code
with the world!

Dont create these methods by hand use define tag.

"))

(defmethod tag-expander (tag atts body)
  (declare (ignore tag atts body))
  (error "~S is not implemented yet" 'tag-expander))

(defun with-tag% (body)
  `(cl-who:htm
    ,body))

(defmacro define-tag ((tag) &body body)
  "Creates tag-p and tag-expander methods for the tag.

The variables tag, body and atts are exposed and can be used by the
body. Use atts to get a list of all the attributes and att to get
an attribute value.

Example:
(define-tag (:w-h)
  `(:h1 :size ,(att :size atts)
        ,@(atts '(:size) atts)
        ,@body))
"
  (unless (keywordp tag)
    (error "tag should be a KEYWORD, not ~S, a ~S" tag (type-of tag)))
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (defmethod tag-p ((tag (eql ,tag)))
       (declare (ignorable tag))
       t)
     (defmethod tag-expander ((tag (eql ,tag)) atts body)
       (declare (ignorable tag atts body))
       (with-tag%
         ,@body))
     ',tag))

