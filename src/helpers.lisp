(in-package :cl-naive-who-ext)

(defun parse-forms% (forms)
  (let ((forms% '()))
    (dolist (form forms)
      (push (parse form) forms%))
    (nreverse forms%)))

(defmacro with-html (&body body)
  "Convenience macro that wraps cl-who:with-html-output to introduce the pre-processing step and also just to cheat with the stream set to *standard-output*."
  (let ((forms (parse-forms% body)))
    `(cl-who:with-html-output (*standard-output* nil :indent t)
       ,@forms)))

(defmacro with-html-to-string (&body body)
  "Convenience macro that wraps cl-who:with-html-output-to-string to introduce the pre-processing step and also just to cheat with the stream set to *standard-output*."
  (let ((forms (parse-forms% body)))
    `(cl-who:with-html-output-to-string (*standard-output* nil :indent t)
       ,@forms)))
