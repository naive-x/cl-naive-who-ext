(defsystem "cl-naive-who-ext.inject.tests"
  :description "Tests for cl-naive-who-ext.inject"
  :version "2021.5.7"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-who-ext.inject :cl-naive-tests :cl-who)
  :components (
	       (:file "tests/package")
	       (:file "tests/tests" :depends-on ("tests/package"))))

